using Plots
using Unitful
using UnitfulRecipes
using NumericalIntegration

theme(:dark)

function impulse_calc(max_deflection = 15u"°")
    if max_deflection == 0
        return 0u"N*s"
    end

    servo_speed = 180u"°/s"

    dt = 0.001u"s"
    time = 0u"s":dt:3.5u"s"

    servo_rate = dt * servo_speed

    # angle = [(t * rate) % max_deflection for t in time]

    angles = zeros(length(time)) .* u"°"
    angle = 0u"°"

    for i = 1:length(time)
        if abs(angle) > max_deflection
            servo_rate = servo_rate * -1
        end

        angle = angle + servo_rate

        angles[i] = angle

    end
    v = angles .|> cos .|> abs
    h = angles .|> sin .|> abs


    horizontal_impulse = let
        vimp = integrate(time, v)
        himp = integrate(time, h)

        himp / (vimp + himp) * 100

    end
    return horizontal_impulse
end

deflections = (0.1:0.1:15)u"°"

impulse = impulse_calc.(deflections)

let
    plot(deflections, impulse, label = "Declected Impulse", legend = :topleft, line = 3)
    yticks!(0:2:12, string.(0:2:12) .* "%")
    ylabel!("Percent of Vertical Impulse Lost")
    xticks!([0:5:20; 7], string.([0:5:20; 7]) .* "°")
    xlabel!("Maximum TVC Deflection")
    title!("Vertical Impulse Deflected\nGiven Max TVC Deflection")
    vline!([7], label = "Current Design", line = (3, :dash, :mediumpurple))
end

savefig("ThrustDef.svg")
