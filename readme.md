# Plots

## Other Plots

- [Simulink Simulation Plots](https://gitlab.com/lander-team/lander-sim)
- [Air Propulsion Plots](https://gitlab.com/lander-team/air-prop-simulation)

## Plots Made Here

![Thrust Deflection](ThrustDef.png)

![TVC Deflection](TVCDef.png)

## Diagrams 

[Mermaid Diagrams](https://gitlab.com/lander-team/concept-of-operations)