using Plots

theme(:dark)

begin
    plot(cosd.(0:30), label = "Vertical Thrust", legend = :topright, line = 3)
    plot!(sind.(0:30), label = "Horizontal Thrust", line = 3)
    vline!([7], label = "Max TVC Deflection", line = (3, :dash))
    # vline!([15], label = "Roll", line = (3, :dash))

    ylabel!("Percent of Thrust")
    xlabel!("Thruster Deflection")
    xvals = [0:5:45; 7]
    xticks!(xvals, string.(xvals) .* "°")
    yticks!(0:0.10:1, string.(0:10:100) .* "%")
    title!("Thrust at TVC Deflections")
end

savefig("TVCDef.svg")
