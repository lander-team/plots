using Plots
using Polynomials
theme(:ggplot2)

zeros = [(0, 1.05), (5, 0.81), (10, 0.74), (15, 0.69), (20, 0.68), (40, 0.64)]
fourfives = [(0, 1.08), (5, 0.91), (10, 0.61), (20, 0.59), (30, 0.72), (40, 0.81)]

fit_line =
    (
        fit([a[1] for a in zeros], [a[2] for a in zeros], 4) + fit([a[1] for a in fourfives], [a[2] for a in fourfives], 4)
    ) ./ 2

begin
    plot(zeros, label = "0° Deflection")
    plot!(fourfives, label = "45° Deflection")

    plot!(fit_line.(0:40), label = "Fit Line", line = (3, :dash))

    title!("Drag Coefficient vs. Vertical Deflection")
    ylabel!("Drag Coefficient")
    xlabel!("Rocket Deflection")
    xticks!(0:5:40, string.(0:5:40) .* "°")
end

savefig("DragvsDeflection.png")