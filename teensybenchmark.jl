using Plots

theme(:juno)

table = [
    ("Teensy 4.1*", 2314),
    ("Teensy 3.6", 440.72),
    ("Sparkfun ESP32", 351.33),
    ("Adafruit HUZZAH 32", 351.35),
    ("Teensy 3.5", 265.50),
    ("Adafruit Metro M4", 214.85),
    ("Teensy 3.2", 168.62),
    ("Arduino Due", 94.95),
    ("Arduino Zero", 56.86),
    ("Arduino Nano", 8.20),
    ("Arduino Mega", 7.03),
]


names = [row[1] for row in table]
performances = [row[2] for row in table]

let
    bar(
        names,
        performances,
        xrotation = 45,
        label = "CoreMark Performance\nLarger is better",
    )
    title!("Performance of Common Microcontrollers")
end

savefig("Teensybenchmark.png")
